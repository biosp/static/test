![Build Status](https://forgemia.inra.fr/biosp/static/test/badges/master/pipeline.svg)

# Site statique de test

- https://docs.gitlab.com/ee/ci/git_submodules.html
- https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html?tab=Multi-project+pipeline#multi-project-pipelines

## Submodules

Ajouter un projet **public** :

```
touch .gitmodules
git submodule add git@forgemia.inra.fr:biosp/static/perso.git
```

Cloner avec l'option `--recursive` :

```
git clone git@forgemia.inra.fr:biosp/static/test.git --recursive
```

ou modifier un dépôt local :

```
git submodule update --init
```
